import java.text.NumberFormat;
/**
 *
 * @author Matthew
 */
// Ex 7-3, step 2: create Rectangle class
public class Rectangle {
    // Ex 7-3, step 3: create variables w/ get and set methods
    private double width, length;
    
    // Ex 7-3, step 4: create constructor and set widt
                    // & height to passed in values
    public Rectangle( double width, double length) {
        this.width = width;
        this.length = length;
    } // end constructor Rectangle
    
    // Ex 7-3, step 5-7: create methods to get the area and perimeter of the
    // given rectangle. Also, return the area and perimeter as String objects
    public double getArea() {
        double area = width * length;
        return area;    
    } // end method getArea
    
    public double getPerimeter() {
        double perimeter = 2 * width + 2 * length;
        return perimeter;
    } // end method getPerimeter
        
    public String doubleToString( double method) {
        NumberFormat number = NumberFormat.getNumberInstance();
        number.setMinimumFractionDigits(3);
        String numberFormatted = number.format(method);
        return numberFormatted; 
    } // end method doubleToString
    
    /**
     * @return the width
     */
    public double getWidth() {
        return width;
    } // end method getWidth

    /**
     * @param width the width to set
     */
    public void setWidth(double width) {
        this.width = width;
    }

    /**
     * @return the length
     */
    public double getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(double length) {
        this.length = length;
    }
} // end classRectangle
