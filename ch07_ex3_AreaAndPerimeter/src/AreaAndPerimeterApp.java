import java.util.Scanner;
// remove "import java.text.NumberFormat;" as it is uneccassary
public class AreaAndPerimeterApp {

    public static void main(String[] args) {
        System.out.println("Welcome to the Area and Perimeter Calculator");
        System.out.println();
        /*test*/
        Scanner sc = new Scanner(System.in);
        String choice = "y";
        while (choice.equalsIgnoreCase("y")) {
            // get input from user
            System.out.print("Enter length: ");
            double length = Double.parseDouble(sc.nextLine());

            System.out.print("Enter width:  ");
            double width = Double.parseDouble(sc.nextLine());

            // calculate total
            // Ex 7-3, step 8: create new Rectangle object
                            // & give it a width & height
            Rectangle rec = new Rectangle(length, width);
            
            // format and display output
            String message =  // Ex 7-3, step 9:
                              // number.format uses rec getArea
                              // & rec getPerimeter methods
                "Area:         " + rec.doubleToString(rec.getArea()) + "\n" +
                "Perimeter:    " + rec.doubleToString(rec.getPerimeter()) + "\n";
            System.out.println(message);

            // see if the user wants to continue
            System.out.print("Continue? (y/n): ");
            choice = sc.nextLine();
            System.out.println();
        }
        System.out.println("Bye!");
    }  
}
